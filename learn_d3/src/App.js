import './App.css';
import LineChart from 'components/LineChart';
import CalendarHeatmap from 'components/CalendarHeatmap';

function App() {
  return (
    <div className="App">
      {/* <LineChart /> */}
      <CalendarHeatmap />
    </div>
  );
}

export default App;
