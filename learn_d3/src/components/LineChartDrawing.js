import {
    axisBottom,
    axisLeft,
    extent,
    line,
    scaleLinear,
    scaleTime,
    select,
} from 'd3'

export default function chartDrawing(chartData, ref) {

    const margin = { top: 20, right: 30, bottom: 30, left: 30 }
    const width = parseInt(select('.line-chart').style('width')) - margin.left - margin.right
    const height = parseInt(select('.line-chart').style('height')) - margin.top - margin.bottom

    const xAxis = scaleTime()
        .domain(extent(chartData.count, (dataPoint) => dataPoint.date))
        .range([0, width])

    const yAxis = scaleLinear()
        .domain([0, chartData.maxCount])
        .range([height, 0])

    const chartSvg = select(ref)
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
            .attr('transform', `translate(${margin.left}, ${margin.top})`)

    chartSvg.append('g')
        .attr('transform', `translate(0, ${height})`)
        .call(axisBottom(xAxis))

    chartSvg.append('g')
        .call(axisLeft(yAxis))

    chartSvg.append('path')
        .datum(chartData.count)
        .attr('fill', 'none')
        .attr('stroke', 'black')
        .attr('stroke-width', 3)
        .attr('d', line()
            .x((dataPoint) => xAxis(dataPoint.date))
            .y((dataPoint) => yAxis(dataPoint.count))
        )

    chartSvg.append('text')
        .attr('x', (width/2))
        .attr('y', (margin.top/5 - 10))
        .attr('text-anchor', 'middle')
        .attr('font-size', '16px')
        .attr('fill', 'black')
        .text('NYC Film Permits entered in 2020')
}