import {
    select,
    scaleLinear
} from 'd3'

export default function drawChart(data, ref) {

    function colorByFreq(freq) {

        return (freq > 80 && freq <= 100)
            ? '#ffffff' : (freq > 60 && freq <= 80)
            ? '#fff7df' : (freq > 40 && freq <= 60)
            ? '#ffe7be' : (freq > 20 && freq <= 40)
            ? '#ffc14d' : '#ff0000'
    }

    const dim = {
        rectSize: 30,
        padding: 2,
        width: 800,
        height: 600,
        margin: { top: 130, left: 50, bottom: 70, right: 50 }
    }

    const chartSvg = select(ref)
        .attr('width', dim.width)
        .attr('height', dim.height)
    
    const xAxis = scaleLinear()
        .domain([0, data.length])
        .range([0, dim.width])

    chartSvg.append('g')
        .selectAll('text')
        .data(data)
        .join('text')
        .text(dataPoint => `${dataPoint.day}`)
        .attr('x', (_, i) => xAxis(i) + dim.margin.left)
        .attr('y', dim.height - dim.margin.bottom)
        .attr('fill', 'white')
        .attr('font-size', 14)

    data.forEach((day, i) => {

        chartSvg.append('g') // am
            .selectAll('rect')
            .data(day.freq.am)
            .join('rect')
            .attr('x', xAxis(i) + dim.margin.left)
            .attr('y', (_, j) => j * (dim.rectSize + dim.padding) + dim.margin.top)
            .attr('width', dim.rectSize)
            .attr('height', dim.rectSize)
            .attr('fill', dataPoint => colorByFreq(dataPoint))

        chartSvg.append('g') // pm
            .selectAll('rect')
            .data(day.freq.pm)
            .join('rect')
            .attr('x', xAxis(i) + dim.margin.left + dim.rectSize + dim.padding)
            .attr('y', (_, j) => j * (dim.rectSize + dim.padding) + dim.margin.top)
            .attr('width', dim.rectSize)
            .attr('height', dim.rectSize)
            .attr('fill', dataPoint => colorByFreq(dataPoint))
    })

    chartSvg.append('text')
        .text('Calendar Heatmap')
        .attr('x', dim.width/2 - 50)
        .attr('y', dim.height/10)
        .attr('fill', 'white')
        .style('font-size', 20)

}