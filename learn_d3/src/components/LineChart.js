import { useEffect, useRef, useState } from 'react'

import getDateFrequency from 'utils/getDateFrequency'
import chartDrawing from './LineChartDrawing'
import './LineChartStyles.css'

export default function LineChart() {

    const chartRef = useRef()
    const [ chartData, setChartData] = useState()

    useEffect(() => {

        async function fetchData(processData) {
            const response = await fetch('https://data.cityofnewyork.us/resource/tg4x-b46p.json')
            const data = await response.json()
            const chartData = await processData(data)
            setChartData(chartData)
        }

        fetchData(getDateFrequency)
    }, [])

    console.log(chartData)

    if(chartRef.current) {
        chartDrawing(chartData, chartRef.current)
    }

    return <div className='line-chart'>
        <svg ref={chartRef}></svg>
    </div>
}
