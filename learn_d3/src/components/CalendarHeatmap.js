import { useRef, useEffect } from 'react'
import drawChart from './CalendarHeatmapDrawing'

import './CalendarHeatmap.css'

export default function() {

    const chartRef = useRef()
    const data = [
        { day: 'Mon', freq: { am: getRandomTime(), pm: getRandomTime() } },
        { day: 'Tue', freq: { am: getRandomTime(), pm: getRandomTime() } },
        { day: 'Wed', freq: { am: getRandomTime(), pm: getRandomTime() } },
        { day: 'Thu', freq: { am: getRandomTime(), pm: getRandomTime() } },
        { day: 'Fri', freq: { am: getRandomTime(), pm: getRandomTime() } },
        { day: 'Sat', freq: { am: getRandomTime(), pm: getRandomTime() } },
        { day: 'Sun', freq: { am: getRandomTime(), pm: getRandomTime() } }
    ]

    useEffect(() => {

        drawChart(data, chartRef.current)
    }, [])

    return <div className='heatmap'>
        <svg ref={chartRef}></svg>
    </div>
}

function getRandomTime() {

    return Array.from(
        { length: 12 },
        () => Math.floor(Math.random()*100) + 1
    )
}