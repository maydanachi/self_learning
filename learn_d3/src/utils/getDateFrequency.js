import { timeParse } from 'd3'

export default async function getDateFrequency(bulkData) {

    const parseDate = timeParse('%Y-%m-%d')

    const { countArr, maxCount } = bulkData.reduce((result, each) => {

        const isPermit = each.eventtype === 'Shooting Permit'

        if(isPermit) {
            const currentDate = each.enteredon.slice(0, 10)
            const isNewDate =  result.dateCount.date !== currentDate
            
            if(isNewDate) {
                const { date, count } = result.dateCount
                const isFirstEntry =
                    date === undefined
                    && count === undefined

                if(isFirstEntry) {
                    result.dateCount = { date: currentDate, count: 1 }
                }
                else {
                    result.countArr.push({ date: parseDate(date), count })
                    result.dateCount = { date: currentDate, count: 1}
                    result.maxCount = result.maxCount
                        ? result.maxCount > count
                            ? result.maxCount
                            : count
                        : count
                }
            }
            else {
                result.dateCount.count++
            }
        }

        return result
    }, {
        countArr: [],
        dateCount: {},
        maxCount: null
    })

    return { count: countArr, maxCount }
}