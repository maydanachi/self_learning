function InventoryViewModel() {
    let self = this
    
    const iconTypes = [
        {icon: 'icon-bone', text: 'Bone'},
        {icon: 'icon-ball', text: 'Ball'},
        {icon: 'icon-circle', text: 'Circle'},
        {icon: 'icon-rabbit', text: 'Rabbit'}
    ]

    self.inventory = ko.observableArray([]);

    self.addItem = function() {
        const randomIndex = Math.floor(
            Math.random() * iconTypes.length
        )
        self.inventory.push(iconTypes[randomIndex])
    }

    self.removeItem = function(data, { target }) {
        const indexToRemove = target.getAttribute('item-index')
        self.inventory.splice(indexToRemove, 1)
    }


}

ko.applyBindings(
    new InventoryViewModel(),
    document.querySelector('#knockout-app')
)