function CounterViewModel() {
    let self = this

    self.firstName = ko.observable('Daniel')
    self.count = ko.observable(0)
    self.increase = function() {
        const currentValue = self.count()
        self.count(currentValue + 1)
    }
    self.decrease = function() {
        const currentValue = self.count()
        self.count(currentValue - 1)
        console.log(self)
    }

    self.dogName = ko.computed(function() {
        return self.count() > 0
        ? 'happy'
        : 'sad'
    })

}

ko.applyBindings(
    new CounterViewModel(),
    document.querySelector('#knockout-app')
)
