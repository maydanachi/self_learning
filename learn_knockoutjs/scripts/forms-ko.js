function FormViewModel() {
    const self = this
    self.firstName = ko.observable("Wayne")
    self.hasBeenSubmitted = ko.observable(false)
    self.firstName.subscribe(function(newValue){
        console.log(newValue)
    })
}

ko.applyBindings(
    new FormViewModel(),
    document.querySelector('#knockout-app')
)