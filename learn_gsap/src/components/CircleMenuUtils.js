import { TweenMax, Power3 } from 'gsap'

const defaultDuration = 0.8

export function tweenChangeSize(ref, isExpand = true) {
    
    const sizeDelta = isExpand ? 200 : 75
    
    console.log(Power3.easeOut)

    TweenMax.to(ref, 0.8, {
        ease: Power3.easeOut,
        height: sizeDelta,
        width: sizeDelta,
    })
}

export function tweenStagger(refs, delay = 0) {
    TweenMax.staggerFrom(refs, defaultDuration, {
            delay: 0.2,
            ease: Power3.easeOut,
            opacity: 0,
            x: 40,
        },
        delay,
    )
}

export function tweenBackground(ref) {

    TweenMax.to(ref, 0,
        { css: { visibility: 'visible' } }
    )
}