import { TweenLite, Power3 } from 'gsap'

export const testimonials = [
    {
      name: 'Julia Cameron',
      title: 'Creative Director, VISA',
      image: 'https://images.unsplash.com/photo-1559624989-7b9303bd9792?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=388&q=80',
      quote:
        'It\'s all good. I was amazed at the quality of the Design. We\'ve seen amazing results already.'
    },
    {
      name: 'Eins Strud',
      title: 'Goodest Boy, CBBH',
      image: 'https://images.unsplash.com/photo-1597633425046-08f5110420b5?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
      quote:
        'I\'ve never seen such a good mannered boy. Would purchase services again. 10/10.'
    },
    {
      name: 'Mark Jacobs',
      title: 'Tech Lead, Google',
      image: 'https://images.unsplash.com/photo-1636809424987-d9843d6c92ab?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80',
      quote:
        'The rebranding has really helped our business. Definitely worth the investment.'
    },
    {
      name: 'Lisa Bearings',
      title: 'Brand Coordinator, Facebook',
      image: 'https://images.unsplash.com/photo-1585664811087-47f65abbad64?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=435&q=80',
      quote:
        'The service was excellent. Absolutely wonderful! A complete redesign did it for us.'
    }
  ];

const imageWidth = 330

export function slideHorizontally(
  ref,
  duration,
  times,
  isTowardsRight = true
) {

  const newPosition = isTowardsRight
  ? -imageWidth * (times - 1)
  : imageWidth * (1 - times)
  
  TweenLite.to(ref, duration, {
    ease: Power3.easeOut,
    x: newPosition
  })
}

export function zoomIn(ref, duration) {

  TweenLite.from(ref, duration, {
    ease: Power3.easeOut,
    scale: 1.2,
  })
}

export function fadeOut(ref, duration) {

  TweenLite.to(ref, duration, {
    opacity: 0,
  })
}

export function fadeIn(ref, duration, delay = 0) {

  TweenLite.to(ref, duration, {
    delay,
    opacity: 1,
  })
}

export function getActiveIndex(current, isTowardsRight, length) {
  
    const updatedIndex = isTowardsRight
        ? current + 1
        : current - 1
    const isOutOfRightBound = updatedIndex > length
    const isOutOfLeftBound = updatedIndex < 1

    if(isOutOfLeftBound) {
      
      return {
        index: length,
        overflowLeft: true,
      }
    }
    else if(isOutOfRightBound) {
      return {
        index: 1,
        overflowRight: true
      }
    }

    return { index: updatedIndex }
}