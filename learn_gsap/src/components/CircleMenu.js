import { useEffect, useRef } from 'react'

import {
    tweenBackground,
    tweenChangeSize,
    tweenStagger,
} from './CircleMenuUtils'
import './CircleMenuStyle.scss'

export default function CircleMenu () {

    const backgroundRef = useRef()
    const circleA = useRef()
    const circleB = useRef()
    const circleC = useRef()
    const baseDelay = 0.4

    const handleExpand = (event) => {
        const { target } = event
        tweenChangeSize(target, true)
    }

    const handleShrink = (event) => {
        const { target } = event
        tweenChangeSize(target, false)
    }

    useEffect(() => {
        tweenBackground(backgroundRef.current)
        tweenStagger([circleA.current, circleB.current, circleC.current], baseDelay)
    }, [])

    return <div className='circle-menu' ref={backgroundRef}>
        <div className='circle-container'>
            <div
                className='circle yellow'
                onMouseEnter={handleExpand}
                onMouseLeave={handleShrink}
                ref={circleA}
            />
            <div
                className='circle blue'
                onMouseEnter={handleExpand}
                onMouseLeave={handleShrink}
                ref={circleB}
            />
            <div
                className='circle red'
                onMouseEnter={handleExpand}
                onMouseLeave={handleShrink}
                ref={circleC}
            />
        </div>
    </div>
}