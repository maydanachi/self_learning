import { useEffect, useRef, useState } from 'react'
import { BsArrowLeft, BsArrowRight } from 'react-icons/bs'

import {
    fadeIn,
    fadeOut,
    getActiveIndex,
    slideHorizontally,
    testimonials,
    zoomIn,
} from './SliderUtils'
import './Slider.scss'

export default function IntersectionScroller() {

    const imagesRef = useRef()
    const testimonialsRef = useRef()
    const [active, setActive] = useState(1)

    useEffect(() => {

        const { children: { 0: first } } = testimonialsRef.current
        fadeIn(first, 0)

    }, [])

    function handleSlide(isTowardsRight) {

        const { current: imgList } = imagesRef
        const { children: testimList } = testimonialsRef.current
        const updated = getActiveIndex(
            active,
            isTowardsRight,
            testimonials.length,
        )
        const hasOverflow =
            updated.overflowLeft
            || updated.overflowRight
        
        if(!hasOverflow) {

            slideHorizontally(imgList, 1, updated.index, isTowardsRight)
            zoomIn(imgList.children[updated.index - 1], 0.8)

            const currentTestimonial = testimList[active - 1]
            const nextTestimonial = testimList[updated.index - 1]

            fadeOut(currentTestimonial, 0.8)
            fadeIn(nextTestimonial, 1)
            setActive(updated.index)
        }
    }


    return <div className='slider'>
        <span
            className='arrow left'
            onClick={() => handleSlide(false)}
        >
            <BsArrowLeft size={'2em'} />
        </span>
        <div className='content'>
            <div className='images'>
                <div className='window'>
                    <div
                        className='container'
                        ref={imagesRef}
                    >
                        {
                            testimonials.map((item, i) =>
                                <img 
                                    key={i}
                                    alt={item.name}
                                    src={item.image}
                                    className={
                                        active === i
                                            ? 'active'
                                            : ''
                                    }
                                />
                            )
                        }
                    </div>
                </div>
            </div>
            <div className='testimonials'>
                <div
                    className='container'
                    ref={testimonialsRef}
                >
                    {
                        testimonials.map((item, i) => {

                            const className = `testimonial ${
                                active === i
                                    ? 'active'
                                    : ''}`

                            return <div
                                key={i}
                                className={className}
                            >
                                <h2 className='quote'>{item.quote}</h2>
                                <h4 className='name'>{item.name}</h4>
                                <h3 className='title'>{item.title}</h3>
                            </div>
                        })
                    }
                </div>
            </div>
        </div>
        <span
            className='arrow right'
            onClick={() => handleSlide(true)}
        >
            <BsArrowRight size={'2em'} />
        </span>
    </div>
}
