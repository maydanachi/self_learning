import { useRef, useEffect } from 'react'
import CSSRulePlugin from 'gsap/CSSRulePlugin'
import { Power2, TimelineLite } from 'gsap'

import './RevealerStyle.scss'

export default function Revealer() {

    const revealerRef = useRef()
    const imageRef = useRef()
    const imageReveal = CSSRulePlugin.getRule('.image-container:after')
    const timeLine = new TimelineLite()

    useEffect(() => {

        timeLine.to(revealerRef.current, 0.5, { visibility: 'visible' })
            .to(imageReveal, 1.4, { width: '0%', ease: Power2.easeInOut })
            .to(imageRef.current, 1.4, { scale: 0.8, ease: Power2.easeInOut, delay: -1.6 }) 
    }, [])

    return <div className='revealer' ref={revealerRef} >
            <div className='image-container'>
                <img src={'/possum.avif'} alt={''} ref={imageRef} />
            </div>
        </div>
}