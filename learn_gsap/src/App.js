import './App.css';
import Slider from 'components/Slider'
// import Revealer from 'components/Revealer';
// import CircleMenu from 'components/CircleMenu'

function App() {
  return (
    <div className="App">
      {/* <CircleMenu/> */}
      {/* <Revealer /> */}
      <Slider />
    </div>
  );
}

export default App;
